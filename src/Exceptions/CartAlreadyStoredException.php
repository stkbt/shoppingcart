<?php

namespace Stkbt\Shoppingcart\Exceptions;

use RuntimeException;

class CartAlreadyStoredException extends RuntimeException {}