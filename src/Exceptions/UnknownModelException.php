<?php

namespace Stkbt\Shoppingcart\Exceptions;

use RuntimeException;

class UnknownModelException extends RuntimeException {}