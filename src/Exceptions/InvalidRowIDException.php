<?php

namespace Stkbt\Shoppingcart\Exceptions;

use RuntimeException;

class InvalidRowIDException extends RuntimeException {}