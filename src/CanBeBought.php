<?php

namespace Stkbt\Shoppingcart;

trait CanBeBought
{

    public function isBuyable() {
        
        return true;
    }
    
    /**
     * Get the identifier of the Buyable item.
     *
     * @return int|string
     */
    public function getBuyableIdentifier()
    {
        return method_exists($this, 'getKey') ? $this->getKey() : $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @return string
     */
    public function getBuyableDescription()
    {
        if($this->title) return $this->title;
        if($this->description) return $this->description;

        return null;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @return float
     */
    public function getBuyablePrice()
    {
        if($this->price) return $this->price;

        return null;
    }
}